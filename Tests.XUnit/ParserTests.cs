using System;
using Xunit;
using Xunit.Abstractions;

using BrightOpen.Samotop.Nerez.Grammar;
using FluentAssertions;
using BrightOpen.Samotop.Nerez.Model;
using System.Linq;

namespace Tests.XUnit
{
    public class ParserTests
    {
        private readonly ITestOutputHelper _out;

        public ParserTests(ITestOutputHelper o)
        {
            _out = o;
        }

        [Theory,
            InlineData("Helo boys\r\n"),
            InlineData("invalid"),
            InlineData(""),
            ]
        public void CanParseSomething(string input)
        {
            var sut = new SamotopScriptParser();

            _out.WriteLine($"Parsing {input}");
            _out.WriteLine(Convert.ToString(sut.Parse(input)));
        }

        [Theory,
            InlineData("Helo boys\r\n", "boys"),
            InlineData("Helo boys.woohoo.com\r\n", "boys.woohoo.com"),
            InlineData("Helo [125.6.5.8]\r\n", "[125.6.5.8]"),
            InlineData("Helo #65564654\r\n", "#65564654"),
            InlineData("Helo s:ok\r\n", "s:ok"),
        ]
        public void CanParseHeloWithDomain(string helo, string guest)
        {
            var sut = new SamotopScriptParser();

            var input = sut.Parse(helo);

            input.Should().ContainSingle();
            input.Single().Command.Name.Should().Be("HELO");
            input.Single().Command.Params.Single().Value.Should().Be(guest);
        }

        [Fact]
        public void CanParseDomain()
        {
            var sut = new SamotopScriptParser();

            var result = sut.ParseDomain("abc.bee.cz");

            result.Should().Be("abc.bee.cz");
        }
    }
}
