using System;
using Xunit;
using Xunit.Abstractions;
using FluentAssertions;

using BrightOpen.Samotop.Nerez.Grammar;
using BrightOpen.Samotop.Nerez.Protocol;
using System.IO;
using System.Threading.Tasks;
using NSubstitute;

namespace Tests.XUnit
{
    public class StreamRemoveTests
    {
        [Fact]
        public async Task HandlesEmptyStream()
        {
            var ms = new MemoryStream();
            await ms.Remove(0);
            ms.Length.Should().Be(0);
            ms.Position.Should().Be(0);
        }

        [Fact]
        public void ComplainsWhenBehindTheStream()
        {
            var ms = new MemoryStream();

            ms.Awaiting(s => s.Remove(5)).Should().Throw<ArgumentOutOfRangeException>()
                .And.ParamName.Should().Be("length");
        }
        [Fact]
        public void ComplainsWhenBeforeTheStream()
        {
            var ms = new MemoryStream();

            ms.Awaiting(s => s.Remove(-5)).Should().Throw<ArgumentOutOfRangeException>()
                .And.ParamName.Should().Be("length");
        }

        [Fact]
        public void WorksWithAnySRWStream()
        {
            var ms = Substitute.For<Stream>();
            ms.CanRead.Returns(true);
            ms.CanWrite.Returns(true);
            ms.CanSeek.Returns(true);

            ms.Awaiting(async s => await s.Remove(0)).Should().NotThrow<ArgumentException>();
        }

        [Fact]
        public void RequiresSeekableStream()
        {
            var ms = Substitute.For<Stream>();
            ms.CanRead.Returns(true);
            ms.CanWrite.Returns(true);
            ms.CanSeek.Returns(false);

            ms.Awaiting(async s => await s.Remove(0)).Should().Throw<ArgumentException>()
                .And.ParamName.Should().Be("stream");
        }

        [Fact]
        public void RequiresReadableStream()
        {
            var ms = Substitute.For<Stream>();
            ms.CanRead.Returns(false);
            ms.CanWrite.Returns(true);
            ms.CanSeek.Returns(true);

            ms.Awaiting(async s => await s.Remove(0)).Should().Throw<ArgumentException>()
                .And.ParamName.Should().Be("stream");
        }

        [Fact]
        public void RequiresWritableStream()
        {
            var ms = Substitute.For<Stream>();
            ms.CanRead.Returns(true);
            ms.CanWrite.Returns(false);
            ms.CanSeek.Returns(true);

            ms.Awaiting(async s => await s.Remove(0)).Should().Throw<ArgumentException>()
                .And.ParamName.Should().Be("stream");
        }
    }
}
