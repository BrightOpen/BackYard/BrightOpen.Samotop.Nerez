using System;
using Xunit;
using Xunit.Abstractions;
using FluentAssertions;
using NSubstitute;

using BrightOpen.Samotop.Nerez.Grammar;
using BrightOpen.Samotop.Nerez.Protocol;
using System.Threading.Tasks;
using System.Linq;

namespace Tests.XUnit
{
    public class StreamSourceTests
    {
        [Fact]
        public async Task CanReadABuffer()
        {
            var sourceMock = Substitute.For<ISourceStream<byte>>();
            sourceMock.Read(Arg.Do<byte[]>(buff => buff[5] = 5), 0, 10).Returns(10);

            var sut = new StreamSource<byte>(sourceMock, 10);

            var result = await sut.Read();

            result.Should().BeEquivalentTo(new[] { 0, 0, 0, 0, 0, 5, 0, 0, 0, 0 });
        }
    }
}
