using System;
using Xunit;
using Xunit.Abstractions;
using FluentAssertions;

using BrightOpen.Samotop.Nerez.Grammar;
using BrightOpen.Samotop.Nerez.Protocol;
using System.IO;
using System.Threading.Tasks;

namespace Tests.XUnit
{
    public class StreamTakeUntilTests
    {
        [Fact]
        public async Task TakesABytesUpToAndIncluding()
        {
            var ms = new MemoryStream(new byte[] { 1, 2, 3, 4 });
            var head = await ms.TakeUntilLast(3);
            head.Should().BeEquivalentTo(new byte[] { 1, 2, 3 });
            ms.Length.Should().Be(1);
            ms.Position.Should().Be(1);
        }

        [Fact]
        public async Task TakesNothingInAbsenceOfBoundary()
        {
            var ms = new MemoryStream(new byte[] { 1, 2, 3, 4 });
            var head = await ms.TakeUntilLast(0);
            head.Should().BeEquivalentTo(new byte[] { });
            ms.Length.Should().Be(4);
            ms.Position.Should().Be(4);
        }

        [Fact]
        public async Task HandlesEmptyStream()
        {
            var ms = new MemoryStream();
            var head = await ms.TakeUntilLast(0);
            head.Should().BeEquivalentTo(new byte[] { });
            ms.Length.Should().Be(0);
            ms.Position.Should().Be(0);
        }

        [Fact]
        public void RequiresWritableStream()
        {
            var ms = new MemoryStream(new byte[0], false);

            ms.Awaiting(async s => await s.TakeUntilLast(0)).Should().Throw<ArgumentException>()
                .And.ParamName.Should().Be("stream");
        }
    }
}
