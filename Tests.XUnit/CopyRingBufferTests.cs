using System;
using Xunit;
using Xunit.Abstractions;
using FluentAssertions;

using BrightOpen.Samotop.Nerez.Grammar;
using BrightOpen.Samotop.Nerez.Protocol;

namespace Tests.XUnit
{
    public class CopyRingBufferTests
    {
        [Fact]
        public void GrowsTinyBuffer()
        {
            var buff = new byte[] { 1 };
            var result = buff.CopyRingBuffer(2, 0, 1);
            result.Should().BeEquivalentTo(new byte[] { 1, 0 });
        }

        [Fact]
        public void GrowsEmptyBuffer()
        {
            var buff = new byte[] { };
            var result = buff.CopyRingBuffer(1, 0, 0);
            result.Should().BeEquivalentTo(new byte[] { 0 });
        }

        [Fact]
        public void GrowsSmallBufferWithDataInTheMiddle()
        {
            var buff = new byte[] { 255, 255, 1, 2, 255, 255 };
            var result = buff.CopyRingBuffer(8, 2, 2);
            result.Should().BeEquivalentTo(new byte[] { 1, 2, 0, 0, 0, 0, 0, 0 });
        }

        [Fact]
        public void GrowsSmallBufferWithDataOverTheEdge()
        {
            var buff = new byte[] { 2, 255, 255, 255, 255, 1 };
            var result = buff.CopyRingBuffer(8, 5, 2);
            result.Should().BeEquivalentTo(new byte[] { 1, 2, 0, 0, 0, 0, 0, 0 });
        }

        [Fact]
        public void MovesHeadToStartOfBuffer()
        {
            var buff = new byte[] { 2, 3, 1 };
            var result = buff.CopyRingBuffer(3, 2, 3);
            result.Should().BeEquivalentTo(new byte[] { 1, 2, 3 });
        }

        [Fact]
        public void ClearsBufferOnZeroLength()
        {
            var buff = new byte[] { 2, 3, 1 };
            var result = buff.CopyRingBuffer(3, 2, 0);
            result.Should().BeEquivalentTo(new byte[] { 0, 0, 0 });
        }

        [Fact]
        public void FailsOnNegativeStart()
        {
            var buff = new byte[] { 1 };
            Action sut = () => buff.CopyRingBuffer(3, -3, 1);
            sut.Should().Throw<ArgumentOutOfRangeException>()
                .Which.ParamName.Should().Be("start");
        }

        [Fact]
        public void FailsOnNegativeLength()
        {
            var buff = new byte[] { 1 };
            Action sut = () => buff.CopyRingBuffer(3, 0, -1);
            sut.Should().Throw<ArgumentOutOfRangeException>()
                .Which.ParamName.Should().Be("length");
        }

        [Fact]
        public void FailsOnLenBehindBuffer()
        {
            var buff = new byte[] { 1 };
            Action sut = () => buff.CopyRingBuffer(3, 0, 10);
            sut.Should().Throw<ArgumentOutOfRangeException>()
                .Which.ParamName.Should().Be("length");
        }

        [Fact]
        public void FailsOnStartBehindBuffer()
        {
            var buff = new byte[] { 1 };
            Action sut = () => buff.CopyRingBuffer(3, 10, 1);
            sut.Should().Throw<ArgumentOutOfRangeException>()
                .Which.ParamName.Should().Be("start");
        }

        [Fact]
        public void FailsOnNullBuffer()
        {
            Action sut = () => (null as byte[]).CopyRingBuffer(3, 10, 1);
            sut.Should().Throw<ArgumentNullException>()
                .Which.ParamName.Should().Be("buffer");
        }
    }
}
