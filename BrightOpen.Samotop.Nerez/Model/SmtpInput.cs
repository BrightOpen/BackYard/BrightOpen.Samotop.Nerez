using System;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct SmtpInput
    {
        internal static SmtpInput Incomplete(Segment s) => new SmtpInput(s, false);
        internal static SmtpInput Valid(Segment s, SmtpCommand command) => new SmtpInput(s, command);
        internal static SmtpInput None(Segment s) => new SmtpInput(s, true);

        private readonly bool _incomplete;
        private readonly SmtpCommand _cmd;
        private readonly Segment _segment;

        public string Text => _segment.Text;
        public SmtpCommand Command => _cmd;

        public bool IsIncomplete => _incomplete;

        public SmtpInput(Segment segment, bool complete)
        {
            _incomplete = !complete;
            this._cmd = SmtpCommand.Unknown(segment.Text);
            this._segment = segment;
        }

        public SmtpInput(Segment segment, SmtpCommand command)
        {
            _incomplete = false;
            this._cmd = command;
            this._segment = segment;
        }

        public override string ToString()
        {
            return (_incomplete ? $"NOOP incomplete command {_segment}" : "")
                    + $"{_cmd}";
        }
    }
}