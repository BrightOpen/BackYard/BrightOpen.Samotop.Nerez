using System;
using System.Net;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct SmtpHost
    {
        internal static readonly SmtpHost None = new SmtpHost();
        internal static SmtpHost Domain(string name) => new SmtpHost(name);
        internal static SmtpHost Other(string label, string literal) => new SmtpHost($"{label}:{literal}");
        internal static SmtpHost parse_ip_addr_numeric(string number) => new SmtpHost(IPAddress.Parse(number).ToString());
        internal static SmtpHost parse_ip_addr_composite(string components) => new SmtpHost(IPAddress.Parse(components).ToString());
        internal static SmtpHost parse_ip_addr_ipv6(string label, string literal) => new SmtpHost(IPAddress.Parse(literal).ToString());

        private string _host;

        public SmtpHost(string name) : this()
        {
            _host = name;
        }

        override public string ToString()
        {
            return _host;
        }
    }
}