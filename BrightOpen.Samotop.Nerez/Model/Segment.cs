using System;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct Segment
    {
        private Position _start;
        private Position _end;
        private string _input;

        public Segment(Position start, Position end, string input)
        {
            _start = start;
            _end = end;
            _input = input;
        }

        public string Text => _input;

        public override string ToString()
        {
            return _start == _end 
                    ? $"{_start}: {_input}" 
                    : $"{_start} - {_end}: {_input}";
        }
    }
}