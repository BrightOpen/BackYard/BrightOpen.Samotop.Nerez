using System;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct Position
    {
        private readonly string fileName;
        private readonly int location;
        private readonly int line;
        private readonly int column;

        public Position(string fileName, int location, int line, int column)
        {
            this.fileName = fileName;
            this.location = location;
            this.line = line;
            this.column = column;
        }

        public override string ToString()
        {
            return $"{fileName} @{location} ({line}:{column})";
        }

        public static bool operator ==(Position p1, Position p2)
        {
            return p1.location == p2.location
                && p1.fileName == p2.fileName
                && p1.line == p2.line
                && p1.column == p2.column;
        }
        public static bool operator !=(Position p1, Position p2)
        {
            return !(p1 == p2);
        }

        public override bool Equals(object obj)
        {
            return (obj as Position?) == this;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}