using System;
using System.Collections.Generic;
using System.Linq;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct SmtpCommand : ISmtpCommand
    {
        internal static readonly SmtpCommand Quit = new SmtpCommand(SmtpCommandName.Quit);
        internal static readonly SmtpCommand Rset = new SmtpCommand(SmtpCommandName.Rset);
        internal static readonly SmtpCommand Noop = new SmtpCommand(SmtpCommandName.Noop);
        internal static readonly SmtpCommand Data = new SmtpCommand(SmtpCommandName.Data);
        internal static readonly SmtpCommand Turn = new SmtpCommand(SmtpCommandName.Turn);
        public static SmtpCommand Unknown(string cmdline) => new SmtpCommand(SmtpCommandName.Unknown, new SmtpParam(cmdline));
        public static SmtpCommand Mail(SmtpPath from) => new SmtpCommand(SmtpCommandName.Mail, new SmtpParam("FROM", ":", from));
        public static SmtpCommand Send(SmtpPath from) => new SmtpCommand(SmtpCommandName.Send, new SmtpParam("FROM", ":", from));
        public static SmtpCommand Soml(SmtpPath from) => new SmtpCommand(SmtpCommandName.Soml, new SmtpParam("FROM", ":", from));
        public static SmtpCommand Saml(SmtpPath from) => new SmtpCommand(SmtpCommandName.Saml, new SmtpParam("FROM", ":", from));
        public static SmtpCommand Rcpt(SmtpPath to) => new SmtpCommand(SmtpCommandName.Rcpt, new SmtpParam("TO", ":", to));
        public static SmtpCommand Helo(SmtpParam guest) => new SmtpCommand(SmtpCommandName.Helo, guest);
        public static SmtpCommand Ehlo(SmtpParam guest) => new SmtpCommand(SmtpCommandName.Ehlo, guest);
        public static SmtpCommand Vrfy(string name) => new SmtpCommand(SmtpCommandName.Vrfy, new SmtpParam(name));
        public static SmtpCommand Expn(string name) => new SmtpCommand(SmtpCommandName.Expn, new SmtpParam(name));
        public static SmtpCommand Help(IList<SmtpParam> input) => new SmtpCommand(SmtpCommandName.Help, input.ToArray());


        private readonly string _name;
        private readonly SmtpParam[] _params;

        public string Name => _name;

        public IEnumerable<SmtpParam> Params => _params;

        private SmtpCommand(string name, params SmtpParam[] @params)
        {
            _name = string.IsNullOrEmpty(name) ? null : name.Substring(0, Math.Min(4, name.Length)).ToUpper();
            _params = @params;
        }

        public override string ToString()
        {
            return (string.IsNullOrEmpty(_name) ? "NOOP unknown command " : _name)
                + (_params == null || _params.Length == 0
                    ? ""
                    : " " + string.Join(" ", _params));
        }
    }

    public struct SmtpParam
    {
        private readonly string _name;
        private readonly string _op;
        private readonly object _value;

        public SmtpParam(object value)
        {
            _name = null;
            _op = null;
            _value = value;
        }
        public SmtpParam(string name, string op, object value)
        {
            _name = name?.ToUpper();
            _op = op == ":" ? ":" : op == "=" ? "=" : string.IsNullOrEmpty(name) ? null : "=";
            _value = value;
        }

        public object Value => _value;
        public object Name => _name;

        public override string ToString()
        {
            return string.IsNullOrEmpty(_name)
                    ? $"{_value}"
                    : $"{_name}{_op}{_value}";
        }
    }

    public struct SmtpCommandName
    {
        public static string Unknown = null;
        public static string Quit = "QUIT";
        public static string Rset = "RSET";
        public static string Noop = "NOOP";
        public static string Mail = "MAIL";
        public static string Send = "SEND";
        public static string Soml = "SOML";
        public static string Saml = "SAML";
        public static string Rcpt = "RCPT";
        public static string Data = "DATA";
        public static string Turn = "TURN";
        public static string Helo = "HELO";
        public static string Ehlo = "EHLO";
        public static string Vrfy = "VRFY";
        public static string Expn = "EXPN";
        public static string Help = "HELP";
    }
}