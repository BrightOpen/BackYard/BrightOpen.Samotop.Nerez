using System.Collections.Generic;

namespace BrightOpen.Samotop.Nerez.Model
{
    public interface ISmtpCommand
    {
        string Name { get; }
        IEnumerable<SmtpParam> Params { get; }
    }
}