using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct SmtpScript : IEnumerable<SmtpInput>
    {
        internal static SmtpScript Set(IEnumerable<SmtpInput> inputs) => new SmtpScript(inputs?.ToArray());

        private readonly IEnumerable<SmtpInput> _inputs;

        public SmtpScript(params SmtpInput[] inputs)
        {
            _inputs = inputs ?? new SmtpInput[0];
        }

        public override string ToString()
        {
            return String.Join("\r\n", _inputs.ToArray()) + "\r\n";
        }

        public IEnumerator<SmtpInput> GetEnumerator()
        {
            return _inputs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}