using System;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct SmtpAddress
    {
        internal static readonly SmtpAddress PostMaster = new SmtpAddress("POSTMASTER", SmtpHost.None);
        internal static readonly SmtpAddress None = new SmtpAddress(null, SmtpHost.None);
        internal static SmtpAddress Mailbox(string name, SmtpHost host) => new SmtpAddress(name, host);

        private readonly string _name;
        private readonly SmtpHost _host;

        public SmtpAddress(string name, SmtpHost host)
        {
            this._name = name;
            this._host = host;
        }

        public override string ToString()
        {
            var h = $"{_host}";            
            return string.IsNullOrEmpty(h)
                ? _name
                : $"{_name}@{h}";
        }
    }
}