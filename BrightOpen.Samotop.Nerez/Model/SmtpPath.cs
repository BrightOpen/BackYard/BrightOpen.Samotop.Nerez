using System;
using System.Collections.Generic;
using System.Linq;

namespace BrightOpen.Samotop.Nerez.Model
{
    public struct SmtpPath
    {
        public static readonly SmtpPath Postmaster = new SmtpPath(true);
        public static readonly SmtpPath Null = new SmtpPath(false);

        public static SmtpPath Relay(IList<SmtpHost> relays, SmtpAddress address) => new SmtpPath(relays?.ToArray(), address);
        public static SmtpPath Direct(SmtpAddress address) => new SmtpPath(null, address);

        private readonly SmtpHost[] _relays;
        private readonly SmtpAddress _address;

        public SmtpPath(SmtpHost[] relays, SmtpAddress address)
        {
            _relays = relays;
            _address = address;
        }
        public SmtpPath(bool isPostMaster) : this()
        {
            _relays = null;
            _address = isPostMaster ? SmtpAddress.PostMaster : SmtpAddress.None;
        }

        public override string ToString()
        {
            return "<" + (

                    _relays == null || _relays.Length == 0
                    ? ""
                    : string.Join(",", _relays.Select(r => $"@{r}")) + ":"

                ) + $"{_address}>";
        }

    }
}