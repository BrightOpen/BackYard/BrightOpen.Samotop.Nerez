﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using BrightOpen.Samotop.Nerez.Grammar;

namespace BrightOpen.Samotop.Nerez.Service
{

    public class SmtpService : IServerVisitor
    {
        public Task ServerError(EndPoint local, Exception exception)
        {
            return Task.CompletedTask;
        }

        public Task<IConnectionVisitor> HandleIncommingConnection(Socket connection)
        {
            //connection.Blocking = false;

            return Task.FromResult((IConnectionVisitor)new SmtpSession());
        }

        public Task Run(Func<Task> call)
        {
            Task.Run(call);
            return Task.CompletedTask;
        }

        public Task ServerShutdown(EndPoint local)
        {
            return Task.CompletedTask;
        }

        public Task ServerStarted(Socket listener)
        {
            return Task.CompletedTask;
        }

        public Task<bool> ShouldAccept(EndPoint local)
        {
            return Task.FromResult(true);
        }
    }
}
