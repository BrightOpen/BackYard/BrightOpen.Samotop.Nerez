﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BrightOpen.Samotop.Nerez.Grammar;

namespace BrightOpen.Samotop.Nerez.Server
{
    public class ThanksService : IServerVisitor, IConnectionVisitor
    {

        public Task ServerError(EndPoint local, Exception exception)
        {
            Console.WriteLine($"Server error ({local}): {exception}");
            return Task.CompletedTask;
        }

        public Task<IConnectionVisitor> HandleIncommingConnection(Socket connection)
        {
            Console.WriteLine($"{connection.RemoteEndPoint} connected");
            return Task.FromResult((IConnectionVisitor)this);
        }

        public Task Run(Func<Task> call)
        {
            Task.Run(call);
            return Task.CompletedTask;
        }

        public Task ServerShutdown(EndPoint local)
        {
            Console.WriteLine($"Server ({local}) shutdown");
            return Task.CompletedTask;
        }

        public Task ConnectionShutdown(EndPoint remote)
        {
            Console.WriteLine($"Connection {remote} shutdown");
            return Task.CompletedTask;
        }

        public Task<bool> HandleInputFrom(EndPoint remote, ArraySegment<byte> data)
        {
            var txt = Encoding.UTF8.GetString(data.Array, data.Offset, data.Count);
            Console.Write(txt);
            return Task.FromResult(true);
        }

        public Task<bool> ShouldWaitForData(EndPoint remote)
        {
            return Task.FromResult(true);
        }

        public Task<int> GetOutputFor(EndPoint remote, ArraySegment<byte> data)
        {
            var thanks = "Thanks\r\n";
            var bytesStored = Encoding.UTF8.GetBytes(thanks, 0, thanks.Length, data.Array, data.Offset);
            return Task.FromResult(bytesStored);
        }

        public Task ServerStarted(Socket listener)
        {
            return Task.CompletedTask;
        }

        public Task<bool> ShouldAccept(EndPoint local)
        {
            Console.WriteLine($"Waiting for a connection on {local}...");
            return Task.FromResult(true);
        }


        public Task<bool> HandleReadError(EndPoint remote, Exception exception)
        {
            Console.WriteLine($"Read failed for {remote}: {exception}");
            return Task.FromResult(false);
        }

        public Task WriteError(EndPoint remote, Exception exception)
        {
            Console.WriteLine($"Write failed for {remote}: {exception}");
            return Task.CompletedTask;
        }

        public byte[] CreateBuffer()
        {
            return new byte[1024];
        }
    }
}
