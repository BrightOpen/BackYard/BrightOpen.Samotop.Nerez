﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BrightOpen.Samotop.Nerez.Grammar;

namespace BrightOpen.Samotop.Nerez.Server
{
    public class LogAdapterService : IServerVisitor
    {
        private readonly IServerVisitor visitor;
        private readonly TextWriter log;

        public LogAdapterService(IServerVisitor visitor, TextWriter log)
        {
            this.visitor = visitor ?? throw new ArgumentNullException(nameof(visitor));
            this.log = log ?? throw new ArgumentNullException(nameof(log));
        }

        public Task ServerError(EndPoint local, Exception exception)
        {
            log.WriteLine($"Server error ({local}): {exception}");
            return visitor.ServerError(local, exception);
        }

        public async Task<IConnectionVisitor> HandleIncommingConnection(Socket connection)
        {
            log.WriteLine($"{connection.RemoteEndPoint} connected");
            return new ConsoleLogAdapterSession(await visitor.HandleIncommingConnection(connection), log);
        }

        public Task Run(Func<Task> call)
        {
            return visitor.Run(call);
        }

        public Task ServerShutdown(EndPoint local)
        {
            log.WriteLine($"Server ({local}) shutdown");
            return visitor.ServerShutdown(local);
        }
        public Task ServerStarted(Socket listener)
        {
            return visitor.ServerStarted(listener);
        }

        public Task<bool> ShouldAccept(EndPoint local)
        {
            Console.WriteLine($"Waiting for a connection on {local}...");
            return visitor.ShouldAccept(local);
        }
    }

    public class ConsoleLogAdapterSession : IConnectionVisitor
    {
        private IConnectionVisitor visitor;
        private TextWriter log;

        public ConsoleLogAdapterSession(IConnectionVisitor visitor, TextWriter log)
        {
            this.visitor = visitor ?? throw new ArgumentNullException(nameof(visitor));
            this.log = log ?? throw new ArgumentNullException(nameof(log));
        }

        public Task ConnectionShutdown(EndPoint remote)
        {
            log.WriteLine($"Connection {remote} shutdown");
            return visitor.ConnectionShutdown(remote);
        }

        public Task<bool> HandleInputFrom(EndPoint remote, ArraySegment<byte> data)
        {
            var txt = Encoding.UTF8.GetString(data.Array, data.Offset, data.Count);
            log.Write(txt);
            return visitor.HandleInputFrom(remote, data);
        }

        public async Task<bool> ShouldWaitForData(EndPoint remote)
        {
            var read = await visitor.ShouldWaitForData(remote);
            //log.WriteLine($"Wait for data? {read}");
            return read;
        }

        public Task<int> GetOutputFor(EndPoint remote, ArraySegment<byte> data)
        {
            return visitor.GetOutputFor(remote, data);
        }


        public Task<bool> HandleReadError(EndPoint remote, Exception exception)
        {
            log.WriteLine($"Read failed for {remote}: {exception}");
            return visitor.HandleReadError(remote, exception);
        }

        public Task WriteError(EndPoint remote, Exception exception)
        {
            log.WriteLine($"Write failed for {remote}: {exception}");
            return visitor.WriteError(remote, exception);
        }

        public byte[] CreateBuffer()
        {
            return visitor.CreateBuffer();
        }
    }
}
