using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BrightOpen.Samotop.Nerez.Grammar;
using BrightOpen.Samotop.Nerez.Protocol;

namespace BrightOpen.Samotop.Nerez.Service
{
    public class SmtpSession : IConnectionVisitor
    {

        readonly MemoryStream _input = new MemoryStream();
        readonly MemoryStream _output = new MemoryStream();

        //if (bufferSize <= 0) throw new ArgumentOutOfRangeException(nameof(bufferSize), "positive value expected");
        int _bufferSize = 1024;

        public Task ConnectionShutdown(EndPoint remote)
        {
            return Task.CompletedTask;
        }

        public async Task<bool> HandleInputFrom(EndPoint remote, ArraySegment<byte> data)
        {
            await _input.WriteAsync(data.Array, data.Offset, data.Count);
            return true;
        }

        public async Task<bool> ShouldWaitForData(EndPoint remote)
        {
            if (_output.Length != 0)
                return false;

            await Task.Delay(100);
            return true;
        }

        public async Task<int> GetOutputFor(EndPoint remote, ArraySegment<byte> data)
        {
            var bytes = await _input.TakeUntilLast((byte)'\n');
            var str = Encoding.UTF8.GetString(bytes);
            var parser = new SamotopScriptParser();
            var items = parser.Parse(str);
            foreach (var item in items)
            {
                var rsp = $"{item}\r\n";
                var outp = Encoding.UTF8.GetBytes(rsp);
                await _output.WriteAsync(outp, 0, outp.Length);
            }
            return await _output.TakeUntilMax((byte)'\n', data);
        }

        public Task<bool> HandleReadError(EndPoint remote, Exception exception)
        {
            return Task.FromResult(false);
        }

        public Task WriteError(EndPoint remote, Exception exception)
        {
            return Task.CompletedTask;
        }

        public byte[] CreateBuffer()
        {
            return new byte[_bufferSize];
        }
    }
}
