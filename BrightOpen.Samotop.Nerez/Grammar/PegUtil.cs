
using BrightOpen.Samotop.Nerez.Model;
using Pegasus.Common;

namespace BrightOpen.Samotop.Nerez.Grammar
{
    internal static class PegUtil
    {
        public static Segment To(this Cursor start, Cursor end)
        {
            return new Segment(
                start.Position(),
                end.Position(),
                end == start
                ? end.Subject.Substring(start.Location)
                : end.Subject.Substring(start.Location, end.Location - start.Location));
        }

        public static Position Position(this Cursor position)
        {
            return new Position(position.FileName, position.Location, position.Line, position.Column);
        }
    }
}