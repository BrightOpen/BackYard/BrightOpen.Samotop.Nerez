using System.Net.Sockets;
using System.Threading.Tasks;

namespace BrightOpen.Samotop.Nerez.Protocol
{
    public class NetSourceStream : ISourceStream<byte>
    {
        private readonly NetworkStream _stream;

        public NetSourceStream(NetworkStream stream)
        {
            _stream = stream ?? throw new System.ArgumentNullException(nameof(stream));
        }

        public async Task<int> Read(byte[] buffer, int index, int length)
        {
            if(!_stream.DataAvailable) return 0;

            return await _stream.ReadAsync(buffer, index, length);
        }
    }
}