using System.IO;
using System.Threading.Tasks;

namespace BrightOpen.Samotop.Nerez.Protocol
{
    public class ByteSourceStream : ISourceStream<byte>
    {
        private readonly Stream _stream;

        public ByteSourceStream(Stream stream)
        {
            _stream = stream ?? throw new System.ArgumentNullException(nameof(stream));
        }

        public async Task<int> Read(byte[] buffer, int index, int length)
        {
            return await _stream.ReadAsync(buffer, index, length);
        }
    }
}