using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace BrightOpen.Samotop.Nerez.Protocol
{
    public class StreamSource<T> : ISource<T>
    {
        private readonly int _size;
        private readonly ISourceStream<T> _input;

        public StreamSource(ISourceStream<T> input, int bufferSize = 1024)
        {
            if (bufferSize < 0) throw new ArgumentOutOfRangeException(nameof(bufferSize), "non-negative number expected");
            _size = bufferSize;
            _input = input;
        }

        public async Task<T[]> Read()
        {
            var buffer = new T[_size];

            var len = await _input.Read(buffer, 0, buffer.Length);

            return buffer.CopyRingBuffer(len, 0, len);
        }
    }

    internal static class BufferUtil
    {

        public static async Task<int> TakeUntilMax(this MemoryStream stream, byte boundary, ArraySegment<byte> destination)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (destination == null)
            {
                throw new ArgumentNullException(nameof(destination));
            }

            if (!stream.CanSeek || !stream.CanRead || !stream.CanWrite)
            {
                throw new ArgumentException("Stream must support Seek() and be R/W", paramName: nameof(stream));
            }

            if (stream.Length == 0)
            {
                return 0;
            }

            // find the first boundary or max
            stream.Seek(0, SeekOrigin.Begin);
            while (stream.Position < destination.Count && stream.ReadByte() != boundary)
            {
                stream.Seek(1, SeekOrigin.Current);
            }

            var split = (int)Math.Min(stream.Position, int.MaxValue);
            stream.Seek(0, SeekOrigin.Begin);

            var len = await stream.ReadAsync(destination.Array, destination.Offset, destination.Count);

            stream.Seek(0, SeekOrigin.End);
            await stream.Remove(len);

            return len;
        }

        public static async Task<byte[]> TakeUntilLast(this MemoryStream stream, byte boundary)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (!stream.CanSeek || !stream.CanRead || !stream.CanWrite)
            {
                throw new ArgumentException("Stream must support Seek() and be R/W", paramName: nameof(stream));
            }

            if (stream.Length == 0)
            {
                return new byte[0];
            }

            // find the last boundary
            stream.Seek(-1, SeekOrigin.End);
            while (stream.ReadByte() != boundary)
            {
                if (stream.Position == 1)
                {
                    // boundary not found
                    stream.Seek(0, SeekOrigin.Begin);
                    break;
                }

                stream.Seek(-2, SeekOrigin.Current);
            }

            var split = (int)Math.Min(stream.Position, int.MaxValue);
            stream.Seek(0, SeekOrigin.Begin);

            var head = new byte[split];
            var len = await stream.ReadAsync(head, 0, split);
            if (len != head.Length)
            {
                head = head.CopyRingBuffer(len, 0, len);
            }

            stream.Seek(0, SeekOrigin.End);
            await stream.Remove(len);

            return head;
        }

        public static async Task Remove(this Stream stream, int length)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (!stream.CanSeek || !stream.CanRead || !stream.CanWrite)
            {
                throw new ArgumentException("Stream must support Seek() and be R/W", paramName: nameof(stream));
            }

            if (length > stream.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "Length is behind the stream");
            }

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "Length is negative");
            }

            var buff = new byte[1024];
            var moved = 0L;
            while (length + moved < stream.Length)
            {
                stream.Seek(length + moved, SeekOrigin.Begin);
                var read = await stream.ReadAsync(buff, 0, buff.Length);

                stream.Seek(moved, SeekOrigin.Begin);
                await stream.WriteAsync(buff, 0, read);

                moved += read;
            }
            stream.SetLength(moved);
            stream.Seek(0, SeekOrigin.End);
            await stream.FlushAsync();
        }

        public static T[] CopyRingBuffer<T>(this T[] buffer, int size, int start, int length)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException(nameof(buffer));
            }
            if (size < buffer.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(size), "expected number at least the source buffer length");
            }
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "non-negative number expected");
            }
            if (start < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(start), "non-negative number expected");
            }
            if (start != 0 && start >= buffer.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(start), "behind the buffer");
            }
            if (length > buffer.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "behind the buffer");
            }

            // calculate end, length of head and tail
            // [ Xxxx ] [x Xxxx] [xxxx X]
            var capacity = buffer.Length;
            var end = start + length;
            var head = end > capacity ? capacity - start : length;
            var tail = end > capacity ? end - capacity : 0;

            // copy head and tail into the beginning of a new buffer
            var newbuff = new T[size];
            Array.ConstrainedCopy(buffer, start, newbuff, 0, head);
            Array.ConstrainedCopy(buffer, 0, newbuff, head, tail);
            return newbuff;
        }
    }
}