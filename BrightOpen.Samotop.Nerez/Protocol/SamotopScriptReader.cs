﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BrightOpen.Samotop.Nerez.Grammar;
using BrightOpen.Samotop.Nerez.Model;

namespace BrightOpen.Samotop.Nerez.Protocol
{
    public class SamotopScriptReader : ISource<SmtpInput>
    {
        private readonly ISource<byte> _input;
        private readonly SamotopScriptParser _parser;
        private readonly Queue<SmtpInput> _queue = new Queue<SmtpInput>();
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(0);
        private readonly MemoryStream _stream = new MemoryStream();

        public SamotopScriptReader(ISource<byte> input, SamotopScriptParser parser)
        {
            _input = input ?? throw new ArgumentNullException(nameof(input));
            _parser = parser ?? throw new ArgumentNullException(nameof(parser));
        }

        public async Task<SmtpInput[]> Read()
        {
            SmtpInput[] input;

            for (input = null; input == null; input = await DequeueAsync())
            {
                await ParseAsync();
            }

            return input;
        }

        async Task<SmtpInput[]> DequeueAsync()
        {
            await _semaphore.WaitAsync();
            try
            {
                return Dequeue();
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private async Task ParseAsync()
        {
            await _semaphore.WaitAsync();
            try
            {
                await Parse();
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private SmtpInput[] Dequeue()
        {
            if (_queue.Count != 0)
            {
                var input = _queue.ToArray();
                _queue.Clear();
                return input;
            }
            else
            {
                return null;
            }
        }

        private async Task Parse()
        {
            var bytes = await _input.Read();
            if (bytes == null || bytes.Length == 0)
            {
                return;
            }

            await _stream.WriteAsync(bytes, 0, bytes.Length);

            bytes = await _stream.TakeUntilLast((byte)'\n');

            foreach (var input in _parser.Parse(Encoding.UTF8.GetString(bytes)))
            {
                _queue.Enqueue(input);
            }
        }
    }
}
