using System.Collections.Generic;
using System.Threading.Tasks;

namespace BrightOpen.Samotop.Nerez.Protocol
{
    public interface ISource<T>
    {
        Task<T[]> Read();
    }
}