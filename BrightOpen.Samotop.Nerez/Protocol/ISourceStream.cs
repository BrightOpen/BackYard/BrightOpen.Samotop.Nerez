using System.Threading.Tasks;

namespace BrightOpen.Samotop.Nerez.Protocol
{
    public interface ISourceStream<T>
    {
        Task<int> Read(T[] buffer, int index, int length);
    }
}