using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

public interface IConnectionVisitor
{
    Task ConnectionShutdown(EndPoint remote);
    Task<bool> HandleInputFrom(EndPoint remote, ArraySegment<byte> data);
    Task<bool> ShouldWaitForData(EndPoint remote);
    Task<int> GetOutputFor(EndPoint remote, ArraySegment<byte> data);
    Task<bool> HandleReadError(EndPoint remote, Exception exception);
    Task WriteError(EndPoint remote, Exception exception);
    byte[] CreateBuffer();
}