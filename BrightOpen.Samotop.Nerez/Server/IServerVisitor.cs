using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

public interface IServerVisitor
{
    Task Run(Func<Task> call);
    Task<IConnectionVisitor> HandleIncommingConnection(Socket connection);
    Task ServerError(EndPoint local, Exception exception);
    Task ServerShutdown(EndPoint local);
    Task ServerStarted(Socket listener);
    Task<bool> ShouldAccept(EndPoint local);
}