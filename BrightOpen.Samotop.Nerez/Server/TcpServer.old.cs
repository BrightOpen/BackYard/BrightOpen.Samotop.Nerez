/*
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// State object for reading client data asynchronously  
public class TcpConnection
{
    public TcpConnection(Socket socket, int bufferSize, IConnectionVisitor visitor)
    {
        if (bufferSize <= 0) throw new ArgumentOutOfRangeException(nameof(bufferSize), "positive value expected");
        _visitor = visitor ?? throw new ArgumentNullException(nameof(visitor));
        _socket = socket ?? throw new ArgumentNullException(nameof(socket));
        _buffer = new byte[bufferSize];
        _text = new StringBuilder();
    }
    private readonly IConnectionVisitor _visitor;
    // Client  socket.  
    private readonly Socket _socket;
    // Receive buffer.  
    private readonly byte[] _buffer;
    // Received data string.  
    private readonly StringBuilder _text;

    internal void Continue()
    {
        Read();
    }

    void Read()
    {
        _socket.BeginReceive(_buffer, 0, _buffer.Length, 0, new AsyncCallback(ReadCallback), null);
    }

    void ReadCallback(IAsyncResult ar)
    {
        var content = String.Empty;

        // Read data from the client socket.   
        int bytesRead = _socket.EndReceive(ar);

        if (bytesRead > 0)
        {
            // There  might be more data, so store the data received so far.  
            _text.Append(Encoding.ASCII.GetString(_buffer, 0, bytesRead));

            // Check for end-of-file tag. If it is not there, read   
            // more data.  
            content = _text.ToString();
            if (content.LastIndexOf("\n") > -1)
            {
                // All the data has been read from the   
                // client. Display it on the console.  
                Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                    content.Length, content);
                // Echo the data back to the client.  
                Send(content);

                _text.Clear();
            }

            // Not all data received. Get more.  
            _socket.BeginReceive(_buffer, 0, _buffer.Length, 0, new AsyncCallback(ReadCallback), null);
        }
        else
        {
            Shutdown();
        }
    }
    void Send(String data)
    {
        // Convert the string data to byte data using ASCII encoding.  
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        // Begin sending the data to the remote device.  
        _socket.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), null);
    }

    void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Complete sending the data to the remote device.  
            int bytesSent = _socket.EndSend(ar);
            Console.WriteLine("Sent {0} bytes to client.", bytesSent);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    void Shutdown()
    {
        Console.WriteLine($"{_socket.RemoteEndPoint} shutdown");
        _socket.Shutdown(SocketShutdown.Both);
        _socket.Close();
    }

}

public class TcpServer
{
    private readonly IServerVisitor _visitor;
    private readonly IPEndPoint _endpoint;
    private readonly Socket _listener;

    // Thread signal.  
    ManualResetEvent _allDone = new ManualResetEvent(false);

    public TcpServer(IServerVisitor visitor, IPEndPoint endpoint)
    {
        _visitor = visitor ?? throw new ArgumentNullException(nameof(visitor));
        _endpoint = endpoint ?? throw new ArgumentNullException(nameof(visitor));
        // Create a TCP/IP socket.  
        _listener = new Socket(endpoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
    }

    public void Start()
    {
        // Bind the socket to the local endpoint and listen for incoming connections.  
        try
        {
            _listener.Bind(_endpoint);
            _listener.Listen(100);

            while (true)
            {
                // Set the event to nonsignaled state.  
                _allDone.Reset();

                // Start an asynchronous socket to listen for connections.  
                Console.WriteLine($"Waiting for a connection on {_endpoint}...");
                _listener.BeginAccept(new AsyncCallback(AcceptCallback), null);

                // Wait until a connection is made before continuing.  
                _allDone.WaitOne();
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    void AcceptCallback(IAsyncResult ar)
    {
        // Signal the main thread to continue.  
        _allDone.Set();

        // Get the socket that handles the client request.  
        var socket = _listener.EndAccept(ar);

        Console.WriteLine($"{socket.RemoteEndPoint} connected");

        var visitor = _visitor.HandleIncommingConnection(socket).Result;

        // Create the state object.  
        var conn = new TcpConnection(socket, 1024, visitor);
        conn.Continue();
    }
}

*/