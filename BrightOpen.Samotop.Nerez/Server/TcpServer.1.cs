using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class TcpServerX : IDisposable
{
    private readonly IServerVisitor _visitor;
    private readonly EndPoint _endpoint;
    private Socket _listener;

    public TcpServerX(IServerVisitor visitor, EndPoint endpoint)
    {
        _visitor = visitor ?? throw new ArgumentNullException(nameof(visitor));
        _endpoint = endpoint ?? throw new ArgumentNullException(nameof(visitor));
        // Create a TCP/IP socket.  
    }

    public async Task Run()
    {
        if (_disposed)
            throw new ObjectDisposedException(GetType().Name);

        var listener = new Socket(_endpoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

        try
        {
            listener.Bind(_endpoint);
            listener.Listen(100);
            _listener = listener;

            await _visitor.ServerStarted(listener);

            while (!_disposed && _listener == listener && await _visitor.ShouldAccept(_endpoint))
            {
                var socket = await listener.AcceptAsync();

                var visitor = await _visitor.HandleIncommingConnection(socket);

                if (visitor != null)
                {
                    await _visitor.Run(new TcpConnectionX(socket, visitor).Continue);
                }
            }
        }
        catch (Exception x)
        {
            await _visitor.ServerError(listener?.LocalEndPoint, x);
        }
        finally
        {
            await Shutdown(listener);
        }
    }


    #region IDisposable Support
    private bool _disposed = false; // To detect redundant calls

    async Task Shutdown(Socket l)
    {
        if (l != null)
        {
            await _visitor.ServerShutdown(l.LocalEndPoint);
            l.Shutdown(SocketShutdown.Both);
            l.Close();
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                // dispose managed state (managed objects).
                Shutdown(_listener).RunSynchronously();
            }

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.

            _disposed = true;
        }
    }

    // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
    // ~TcpServer2() {
    //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
    //   Dispose(false);
    // }

    // This code added to correctly implement the disposable pattern.
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
        // TODO: uncomment the following line if the finalizer is overridden above.
        // GC.SuppressFinalize(this);
    }
    #endregion
}

public class TcpConnectionX : IDisposable
{
    public TcpConnectionX(Socket socket, IConnectionVisitor visitor)
    {
        _visitor = visitor ?? throw new ArgumentNullException(nameof(visitor));
        _socket = socket ?? throw new ArgumentNullException(nameof(socket));
        _buffer = _visitor.CreateBuffer();
    }
    private readonly IConnectionVisitor _visitor;
    // Client  socket.  
    private readonly Socket _socket;
    // Receive buffer.  
    private readonly byte[] _buffer;
    // Received data string.  

    internal async Task Continue()
    {
        try
        {
            while (!_disposed && await Read())
            {
                await Write();
            }
        }
        finally
        {
            await Shutdown(_socket);
        }
    }

    private async Task Write()
    {
        try
        {
            var segment = new ArraySegment<byte>(_buffer);

            var bytesGiven = await _visitor.GetOutputFor(_socket.RemoteEndPoint, segment);

            if (bytesGiven == 0)
                return;

            await _socket.SendAsync(new ArraySegment<byte>(_buffer, 0, bytesGiven), SocketFlags.None);
        }
        catch (Exception x)
        {
            await _visitor.WriteError(_socket.RemoteEndPoint, x);
        }
    }

    private async Task<bool> Read()
    {
        var read = await _visitor.ShouldWaitForData(_socket.RemoteEndPoint);
        try
        {
            if (read)
            {
                var bytesRead = await _socket.ReceiveAsync(new ArraySegment<byte>(_buffer), SocketFlags.None);
                if (bytesRead == 0)
                {
                    await Shutdown(_socket);
                    return false;
                }
                else
                {
                    return await _visitor.HandleInputFrom(_socket.RemoteEndPoint, new ArraySegment<byte>(_buffer, 0, bytesRead));
                }
            }
            else
            {
                return true;
            }
        }
        catch (Exception x)
        {
            return await _visitor.HandleReadError(_socket.RemoteEndPoint, x);
        }
    }


    async Task Shutdown(Socket socket)
    {
        if (socket != null)
        {
            await _visitor.ConnectionShutdown(socket.RemoteEndPoint);
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }

    #region IDisposable Support
    private bool _disposed = false; // To detect redundant calls

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                // dispose managed state (managed objects).
                Shutdown(_socket).RunSynchronously();
            }

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.

            _disposed = true;
        }
    }

    // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
    // ~TcpConnection2() {
    //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
    //   Dispose(false);
    // }

    // This code added to correctly implement the disposable pattern.
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
        // TODO: uncomment the following line if the finalizer is overridden above.
        // GC.SuppressFinalize(this);
    }
    #endregion

}