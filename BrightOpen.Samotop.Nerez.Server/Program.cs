﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using BrightOpen.Samotop.Nerez.Grammar;
using BrightOpen.Samotop.Nerez.Service;

namespace BrightOpen.Samotop.Nerez.Server
{
    public class Program
    {
        static async Task<int> Main(string[] args)
        {
            if (args == null || args.Length == 0)
                args = new[] { Dns.GetHostName() };

            var svc = new LogAdapterService( new SmtpService(), Console.Out);
            
            var tasks = GetEndpoints(args, 12345)
                        .Select(ep => new TcpServer(svc, ep).Run())
                        .ToArray();

            await Task.WhenAll(tasks);

            return 0;
        }

        static IEnumerable<EndPoint> GetEndpoints(IEnumerable<string> addrs, int defaultPort)
        {
            return addrs.SelectMany(a => ParseIPEndPoint(a, defaultPort)).Distinct();
        }

        private static IEnumerable<IPEndPoint> ParseIPEndPoint(string text, int defaultPort)
        {
            Uri uri;
            if (Uri.TryCreate(text, UriKind.Absolute, out uri)
                || Uri.TryCreate(String.Concat("ip://", text), UriKind.Absolute, out uri)
                || Uri.TryCreate(String.Concat("ip://", $"[{text}]"), UriKind.Absolute, out uri))
            {
                var port = uri.Port < 0 ? defaultPort : uri.Port;

                IPAddress ip;
                if (IPAddress.TryParse(uri.Host, out ip))
                {
                    return new[] { new IPEndPoint(ip, port) };
                }
                else
                {
                    return Dns.GetHostEntry(uri.DnsSafeHost)
                        ?.AddressList.Select(ipa => new IPEndPoint(ipa, port))
                        ?? throw new FormatException("Failed to parse text to IPEndPoint");
                }
            }
            else
            {
                throw new FormatException("Failed to parse text to IPEndPoint");
            }
        }

        /*

                var tests = new List<string>(){
                    "Helo boys\r\n",
                    "help a=x b c=d\r\n",
                    "help a:x b c=d\r\n",
                    "mail fRoM:<abc@def.com>\r\n",
                    "quit\r\n\r\ninvalid\nincomp",
                    "invalid\r\n"
                };

                var parser = new SamotopScriptParser();

                foreach (var t in tests)
                {
                    Console.WriteLine($"Parsing {t}");
                    Console.Write(parser.Parse(t));
                }
                 */
    }
}